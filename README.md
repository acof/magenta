# traceroute 1.1.1.1

when problem occurs at 21:00. data from 2 nights

## ipv4

### 0

```
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  192.168.1.1 (192.168.1.1)  1.651 ms  1.619 ms  2.399 ms
 2  compalhub.home (192.168.0.1)  7.059 ms  7.046 ms  7.820 ms
 3  84.116.231.35 (84.116.231.35)  105.752 ms  104.573 ms  106.415 ms
 4  at-vie05d-rc1-ae-11-0.aorta.net (84.116.228.205)  41.900 ms  46.672 ms  47.376 ms
 5  at-vie05b-ri3-ae-12-0.aorta.net (213.46.173.125)  105.682 ms  105.669 ms  107.180 ms
 6  ix-et-25.hcore1.wi4-vienna.as6453.net (195.219.25.0)  83.459 ms  57.243 ms  58.028 ms
 7  195.219.25.10 (195.219.25.10)  39.708 ms  46.472 ms  47.144 ms
 8  one.one.one.one (1.1.1.1)  55.928 ms  57.675 ms  58.355 ms
 ```

 ### 1

```
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  192.168.1.1 (192.168.1.1)  5.294 ms  5.262 ms  5.249 ms
 2  compalhub.home (192.168.0.1)  6.315 ms  6.301 ms  6.288 ms
 3  84.116.231.35 (84.116.231.35)  103.953 ms  108.459 ms  108.445 ms
 4  at-vie05d-rc1-ae-11-0.aorta.net (84.116.228.205)  70.258 ms  72.143 ms  72.130 ms
 5  at-vie05b-ri3-ae-12-0.aorta.net (213.46.173.125)  83.843 ms  83.831 ms  91.125 ms
 6  ix-et-25.hcore1.wi4-vienna.as6453.net (195.219.25.0)  79.399 ms  64.212 ms  70.891 ms
 7  195.219.25.10 (195.219.25.10)  58.139 ms  70.485 ms  70.467 ms
 8  one.one.one.one (1.1.1.1)  78.016 ms  65.029 ms  65.823 ms
```

### 2

```
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets
 1  192.168.1.1 (192.168.1.1)  3.747 ms  3.725 ms  3.714 ms
 2  compalhub.home (192.168.0.1)  10.068 ms  10.058 ms  10.048 ms
 3  84.116.231.35 (84.116.231.35)  97.645 ms  110.767 ms  112.904 ms
 4  at-vie05d-rc1-ae-11-0.aorta.net (84.116.228.205)  89.738 ms  89.728 ms  89.718 ms
 5  at-vie05b-ri3-ae-12-0.aorta.net (213.46.173.125)  110.718 ms  112.854 ms  112.845 ms
 6  ix-et-25.hcore1.wi4-vienna.as6453.net (195.219.25.0)  90.244 ms  82.372 ms  82.354 ms
 7  195.219.25.10 (195.219.25.10)  111.779 ms  111.496 ms  111.482 ms
 8  one.one.one.one (1.1.1.1)  84.157 ms  53.626 ms  53.605 ms
 ```

 ## ipv6

```
traceroute to one.one.one.one (2606:4700:4700::1001), 30 hops max, 80 byte packets
 1  2a02-8388-c080-8bf0-86d8-1bff-fedc-d5cc.cable.dynamic.v6.surfer.at (2a02:8388:c080:8bf0:86d8:1bff:fedc:d5cc)  1.940 ms  1.909 ms  1.894 ms
 2  compalhub.home (2a02:8388:c080:8b80:362c:c4ff:fe1a:a2f0)  6.637 ms  7.341 ms  7.327 ms
 3  * * *
 4  at-klu-vill-pe02-bundle-ether301-6.v6.aorta.net (2a02:8380:106:9::1)  26.957 ms  29.280 ms  29.266 ms
 5  at-vie05d-rc1-lo0-0.v6.aorta.net (2001:730:2800::5474:807e)  30.450 ms  29.806 ms  39.573 ms
 6  at-vie05b-ri3-lo0-0.v6.aorta.net (2001:730:2800::5474:8014)  40.707 ms  38.067 ms  39.422 ms
 7  ix-et-25.hcore1.wi4-vienna.ipv6.as6453.net (2a01:3e0:1e00::a)  51.094 ms  48.773 ms  48.144 ms
 8  2a01:3e0:1e00::2 (2a01:3e0:1e00::2)  34.223 ms  31.925 ms  30.284 ms
 9  2400:cb00:114:1024::ac44:313d (2400:cb00:114:1024::ac44:313d)  30.253 ms 2400:cb00:114:1024::ac44:310d (2400:cb00:114:1024::ac44:310d)  28.564 ms 2400:cb00:114:1024::ac44:3124 (2400:cb00:114:1024::ac44:3124)  31.355 ms
```

# traceroute 195.34.133.21

```
traceroute to 195.34.133.21 (195.34.133.21), 30 hops max, 60 byte packets
 1  192.168.1.1 (192.168.1.1)  1.726 ms  2.278 ms  2.841 ms
 2  compalhub.home (192.168.0.1)  10.825 ms  10.815 ms *
 3  84.116.231.35 (84.116.231.35)  106.751 ms  113.247 ms  113.237 ms
 4  at-vie05d-rc1-ae-11-0.aorta.net (84.116.228.205)  94.224 ms  94.861 ms  94.851 ms
 5  at-vie01b-rc1-ae-29-0.aorta.net (84.116.140.1)  117.663 ms  117.654 ms  120.080 ms
 6  at-vie12c-ra1-be-2.aorta.net (213.46.173.9)  150.937 ms  128.932 ms  134.166 ms
 7  * * *
 8  * * *
 9  * * *
10  * * *
11  * * *
12  * * *
13  * * *
14  * * *
15  * * *
16  * * *
17  * *
<cancelled>
```

# pings

## ping -4 one.one.one.one

```
PING  (1.0.0.1) 56(84) bytes of data.
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=1 ttl=57 time=89.2 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=2 ttl=57 time=69.3 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=3 ttl=57 time=94.3 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=4 ttl=57 time=71.6 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=5 ttl=57 time=81.4 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=6 ttl=57 time=88.2 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=7 ttl=57 time=101 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=8 ttl=57 time=92.6 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=9 ttl=57 time=93.7 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=10 ttl=57 time=105 ms
64 bytes from one.one.one.one (1.0.0.1): icmp_seq=11 ttl=57 time=108 ms
```

## ping -6 one.one.one.one

```
PING one.one.one.one(one.one.one.one (2606:4700:4700::1001)) 56 data bytes
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=1 ttl=57 time=15.1 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=2 ttl=57 time=14.9 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=3 ttl=57 time=15.0 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=4 ttl=57 time=16.0 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=5 ttl=57 time=28.6 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=6 ttl=57 time=15.8 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=7 ttl=57 time=14.4 ms
64 bytes from one.one.one.one (2606:4700:4700::1001): icmp_seq=8 ttl=57 time=16.0 ms

--- one.one.one.one ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7009ms
rtt min/avg/max/mdev = 14.425/16.967/28.556/4.413 ms
```

## ping 195.34.133.21

```
PING 195.34.133.21 (195.34.133.21) 56(84) bytes of data.
64 bytes from 195.34.133.21: icmp_seq=1 ttl=58 time=62.4 ms
64 bytes from 195.34.133.21: icmp_seq=2 ttl=58 time=69.1 ms
64 bytes from 195.34.133.21: icmp_seq=3 ttl=58 time=91.7 ms
64 bytes from 195.34.133.21: icmp_seq=4 ttl=58 time=75.0 ms
64 bytes from 195.34.133.21: icmp_seq=5 ttl=58 time=80.7 ms
64 bytes from 195.34.133.21: icmp_seq=6 ttl=58 time=77.7 ms
64 bytes from 195.34.133.21: icmp_seq=7 ttl=58 time=104 ms
64 bytes from 195.34.133.21: icmp_seq=8 ttl=58 time=68.4 ms
64 bytes from 195.34.133.21: icmp_seq=9 ttl=58 time=61.7 ms
64 bytes from 195.34.133.21: icmp_seq=10 ttl=58 time=57.8 ms
64 bytes from 195.34.133.21: icmp_seq=11 ttl=58 time=54.5 ms

--- 195.34.133.21 ping statistics ---
11 packets transmitted, 11 received, 0% packet loss, time 10013ms
rtt min/avg/max/mdev = 54.521/73.003/103.888/14.261 ms
```

